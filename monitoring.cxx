/* monitoring.cxx */
#include <stdio.h>
#include <stdlib.h>

#include "ndds/ndds_cpp.h"

/* Delete all entities */
static int appShutdown(
    DDSDomainParticipant * participant)
{
    DDS_ReturnCode_t retCode;
    int status = 0;

    if (participant != NULL) {
        retCode = participant->delete_contained_entities();
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retCode);
            status = -1;
        }

        retCode = DDSTheParticipantFactory->delete_participant(participant);
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retCode);
            status = -1;
        }
    }
    
    /* Release memory associated with participant factory */
    retCode = DDSDomainParticipantFactory::finalize_instance();
    if (retCode != DDS_RETCODE_OK) {
        fprintf(stderr, "finalize_instance error %d\n", retCode);
        status = -1;
    }
    return status;
}

int main(int argc, char *argv[])
{

    int sampleCount = 0; // infinite loop
    if (argc >= 2) {
        sampleCount = atoi(argv[2]);
    }
    
    DDS_ReturnCode_t retCode;
    int count = 0;  
    DDS_Duration_t sendPeriod = {1,0};
    DDS_ReturnCode_t lastSetError = DDS_RETCODE_OK;

    DDS_DomainParticipantFactoryQos factory_qos;

    DDSTheParticipantFactory->get_qos(factory_qos);
    factory_qos.profile.url_profile.ensure_length(1, 1);
    factory_qos.profile.url_profile[0] = DDS_String_dup("hc_system.xml");
    DDSTheParticipantFactory->set_qos(factory_qos);

    ////////////////////////////////
    //Applications Creation from XML
    /* Monitoring App Creation*/
    DDSDomainParticipant * monitoring_participant = DDSTheParticipantFactory->
        create_participant_from_config("SCO_Components::Monitoring_app");
    if (monitoring_participant == NULL) {
        fprintf(stderr, "create_participant_from_config error MD_participant\n");
        appShutdown(monitoring_participant);
        return -1;
    }       
    ////////////////////////////////



   ////////////////////////////////////////////////////////////////
    //Business Logic
    ////////////////////////////////////////////////////////////////
    /* Main loop */
    for (count=0; (sampleCount == 0) || (count < sampleCount); ++count) {
       
        NDDSUtility::sleep(sendPeriod);
    }


    /* Delete all entities */
    return appShutdown(monitoring_participant);
}

