/* control_app.cxx */
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "ndds/ndds_cpp.h"



class CurrentStateListener: public DDSDataReaderListener {

public:
    CurrentStateListener(){
       
    }
    ~CurrentStateListener(){
        
    }


    void on_data_available(DDSDataReader* reader) {

        
        DDS_DynamicDataSeq dataSeq;
        DDS_SampleInfoSeq infoSeq;
        DDS_ReturnCode_t rc;

        
        DDSDynamicDataReader *currentStateReader = DDSDynamicDataReader::narrow(reader);
        if (currentStateReader == NULL) {
            std::cerr << "! Unable to narrow data reader" << std::endl;
            return;
        }

        rc = currentStateReader->take(
                            dataSeq, 
                            infoSeq, 
                            DDS_LENGTH_UNLIMITED,
                            DDS_ANY_SAMPLE_STATE, 
                            DDS_ANY_VIEW_STATE, 
                            DDS_ANY_INSTANCE_STATE);
        if (rc == DDS_RETCODE_NO_DATA) {
            return;
        } else if (rc != DDS_RETCODE_OK) {
            std::cerr << "! Unable to take data from data reader, error " 
                      << rc << std::endl;
            return;
        }

        for (int i = 0; i < dataSeq.length(); ++i) {
            if (infoSeq[i].valid_data) {
                DDS_DynamicData& instance = dataSeq[i];
                DDS_UnsignedLong length=500;
                char* rep = new char[length];
                DDS_PrintFormatProperty format;
                instance.to_string(rep, length, format);
                std::cout<<"Status sample received: \n"<<rep<<std::endl;
                delete[] rep;
                rep = NULL;
            }
        }
    
        rc = currentStateReader->return_loan(dataSeq, infoSeq);
        if (rc != DDS_RETCODE_OK) {
            std::cerr << "! Unable to return loan, error "
                      << rc << std::endl;
        }
    }
};  // End of class CurrentStateListener

class ObjectiveStateListener: public DDSDataReaderListener {

public:
    ObjectiveStateListener(){
       
    }
    ~ObjectiveStateListener(){
        
    }


    void on_data_available(DDSDataReader* reader) {

        
        DDS_DynamicDataSeq dataSeq;
        DDS_SampleInfoSeq infoSeq;
        DDS_ReturnCode_t rc;

        
        DDSDynamicDataReader *objectiveStateReader = DDSDynamicDataReader::narrow(reader);
        if (objectiveStateReader == NULL) {
            std::cerr << "! Unable to narrow data reader" << std::endl;
            return;
        }

        rc = objectiveStateReader->take(
                            dataSeq, 
                            infoSeq, 
                            DDS_LENGTH_UNLIMITED,
                            DDS_ANY_SAMPLE_STATE, 
                            DDS_ANY_VIEW_STATE, 
                            DDS_ANY_INSTANCE_STATE);
        if (rc == DDS_RETCODE_NO_DATA) {
            return;
        } else if (rc != DDS_RETCODE_OK) {
            std::cerr << "! Unable to take data from data reader, error " 
                      << rc << std::endl;
            return;
        }

        for (int i = 0; i < dataSeq.length(); ++i) {
            if (infoSeq[i].valid_data) {
                DDS_DynamicData& instance = dataSeq[i];
                DDS_UnsignedLong length=500;
                char* rep = new char[length];
                DDS_PrintFormatProperty format;
                instance.to_string(rep, length, format);
                std::cout<<"Objective status sample received: \n"<<rep<<std::endl;
                delete[] rep;
                rep = NULL;
            }
        }
    
        rc = objectiveStateReader->return_loan(dataSeq, infoSeq);
        if (rc != DDS_RETCODE_OK) {
            std::cerr << "! Unable to return loan, error "
                      << rc << std::endl;
        }
    }
};  // End of class ObjectiveStateListener

/* Delete all entities */
static int appShutdown(
    DDSDomainParticipant * participant)
{
    DDS_ReturnCode_t retCode;
    int status = 0;

    if (participant != NULL) {
        retCode = participant->delete_contained_entities();
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retCode);
            status = -1;
        }

        retCode = DDSTheParticipantFactory->delete_participant(participant);
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retCode);
            status = -1;
        }
    }
    
    /* Release memory associated with participant factory */
    retCode = DDSDomainParticipantFactory::finalize_instance();
    if (retCode != DDS_RETCODE_OK) {
        fprintf(stderr, "finalize_instance error %d\n", retCode);
        status = -1;
    }
    return status;
}

int main(int argc, char *argv[])
{

    int sampleCount = 0; // infinite loop
    if (argc >= 2) {
        sampleCount = atoi(argv[2]);
    }
    DDS_ReturnCode_t retCode;
    int count = 0;  
    DDS_Duration_t sendPeriod = {20,0};
    DDS_ReturnCode_t lastSetError = DDS_RETCODE_OK;

    DDS_DomainParticipantFactoryQos factory_qos;

    DDSTheParticipantFactory->get_qos(factory_qos);
    factory_qos.profile.url_profile.ensure_length(1, 1);
    factory_qos.profile.url_profile[0] = DDS_String_dup("hc_system.xml");
    DDSTheParticipantFactory->set_qos(factory_qos);

    ////////////////////////////////
    //Applications Creation from XML
    /* Control App Creation*/    
    DDSDomainParticipant * control_participant = DDSTheParticipantFactory->
            create_participant_from_config("SCO_Components::Control_app");
    if (control_participant == NULL) {
        fprintf(stderr, "create_participant_from_config error control_participant\n");
        appShutdown(control_participant);
        return -1;
    }

    CurrentStateListener listener;
    ////////////////////////////////
    /* Current State Reader */
    DDSDynamicDataReader* current_state_reader = DDSDynamicDataReader::narrow(
            control_participant->lookup_datareader_by_name(
                                      "control_app_Sub::current_state_reader"));
    if (current_state_reader == NULL) {
        fprintf(stderr, "lookup_datareader_by_name error current_state_reader\n");
        appShutdown(control_participant);
        return -1;
    }

    current_state_reader->set_listener(&listener, DDS_STATUS_MASK_ALL);


    ObjectiveStateListener olistener;
    ///////////////////////////////
    /* Objective State Reader */
    DDSDynamicDataReader* objective_reader = DDSDynamicDataReader::narrow(
            control_participant->lookup_datareader_by_name(
                                      "control_app_Sub::objective_reader"));
    if (objective_reader == NULL) {
        fprintf(stderr, "lookup_datareader_by_name error objective_reader\n");
        appShutdown(control_participant);
        return -1;
    }

    objective_reader->set_listener(&olistener, DDS_STATUS_MASK_ALL);
  
    ////////////////////////////////
    /* Control Writer */
    DDSDynamicDataWriter * control_writer = DDSDynamicDataWriter::narrow(
            control_participant->lookup_datawriter_by_name(
                                      "control_app_Pub::control_Writer"));
    if (control_writer == NULL) {
        fprintf(stderr, "lookup_datawriter_by_name error control_writer\n");
        appShutdown(control_participant);
        return -1;
    }

    DDS_DynamicData * control_data = 
            control_writer->create_data(DDS_DYNAMIC_DATA_PROPERTY_DEFAULT);
    if (control_data == NULL) {
        fprintf(stderr, "create_data error control_data\n");
        appShutdown(control_participant);
        return -1;
    }
       
    ////////////////////////////////
    /* Main loop */
    ////////////////////////////////
    for (count=0; (sampleCount == 0) || (count < sampleCount); ++count) {

        //Business Logic for Control writer
        printf("Writing control sample count: %d\n", count);
        fflush(stdout);
        
        if (count%2==0){
            /* Set the data fields for test_1 */
            retCode = control_data->set_string("command_name", 
                                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                                            "test_1");
            if ( retCode != DDS_RETCODE_OK) { lastSetError = retCode; }
    
                if ( lastSetError != DDS_RETCODE_OK ) {            
                fprintf(stderr, "Error setting dynamic data: %d\n", lastSetError);
                appShutdown(control_participant);
                return -1;
            }   
            retCode = control_data->set_short("command_id", 
                                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                                            count);
            if ( retCode != DDS_RETCODE_OK) { lastSetError = retCode; }
    
                if ( lastSetError != DDS_RETCODE_OK ) {            
                fprintf(stderr, "Error setting dynamic data: %d\n", lastSetError);
                appShutdown(control_participant);
                return -1;
            } 
        } else {
            /* Set the data fields for insuline level */
            retCode = control_data->set_string("command_name", 
                                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                                            "insuline_level");
            if ( retCode != DDS_RETCODE_OK) { lastSetError = retCode; }
    
                if ( lastSetError != DDS_RETCODE_OK ) {            
                fprintf(stderr, "Error setting dynamic data: %d\n", lastSetError);
                appShutdown(control_participant);
                return -1;
            }   
            retCode = control_data->set_short("command_id", 
                                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                                            count);
            if ( retCode != DDS_RETCODE_OK) { lastSetError = retCode; }
    
                if ( lastSetError != DDS_RETCODE_OK ) {            
                fprintf(stderr, "Error setting dynamic data: %d\n", lastSetError);
                appShutdown(control_participant);
                return -1;
            } 
            retCode = control_data->set_long("value", 
                                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, 
                                            6);
            if ( retCode != DDS_RETCODE_OK) { lastSetError = retCode; }
    
                if ( lastSetError != DDS_RETCODE_OK ) {            
                fprintf(stderr, "Error setting dynamic data: %d\n", lastSetError);
                appShutdown(control_participant);
                return -1;
            } 
        }
       
        retCode = control_writer->write(*control_data, DDS_HANDLE_NIL);
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "write error %d\n", retCode);
            appShutdown(control_participant);
            return -1;
        }
        NDDSUtility::sleep(sendPeriod);
    }
    
    /* Delete data sample */
    control_writer->delete_data(control_data);

    /* Delete all entities */
    return appShutdown(control_participant);
}

