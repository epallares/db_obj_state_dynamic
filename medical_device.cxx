/* medical_device.cxx */
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include "ndds/ndds_cpp.h"

enum ControlCommand { NONE, TEST_1, INSULINE_LEVEL };
enum OperationalStatus {NOP, IDLE, TAKING_SAMPLE, ANALYZING, RESULTS};
enum ObjectiveStatus{WAITING, PROCESSING, SUCCESS, FAIL};


class ControlListener: public DDSDataReaderListener {
private:
    ControlCommand current_command;
    OperationalStatus operational_status;
    ObjectiveStatus objective_status;

    int insuline_level;
    int current_insuline_level;

    //Current command that is being executed. Command id and command name is being stored.
    char* command_name;
    short command_id;

public:
    ControlListener(){
        current_command=NONE;
        operational_status=NOP;
        objective_status=WAITING;

        command_name = NULL;
        command_id = -1;

        insuline_level=-1;
        current_insuline_level=-1;
    }
    ~ControlListener(){
        if (command_name!=NULL){
            delete command_name;
            command_name=NULL;
        }
    }

    ControlCommand getCurrentControlCommand(){
        return current_command;
    }

    void setCurrentControlCommand(ControlCommand command){
        current_command = command;
    }
    OperationalStatus getCurrentOperationalStatus(){
        return operational_status;
    }

    void setCurrentOperationalStatus(OperationalStatus command){
        operational_status = command;
    }

    ObjectiveStatus getCurrentObjectiveStatus(){
        return objective_status;
    }

    void setCurrentObjectiveStatus(ObjectiveStatus command){
        objective_status = command;
    }

    char* getCurrentCommandName(){
        return command_name;
    }

    short getCurrentCommandId(){
        return command_id;
    }

    int getInsulineLevel(){
        return insuline_level;
    }
    int getCurrentInsulineLevel(){
        return current_insuline_level;
    }
    void setCurrentInsulineLevel(int current){
        current_insuline_level=current;
    }


void on_data_available(DDSDataReader* reader) {

        
        DDS_DynamicDataSeq dataSeq;
        DDS_SampleInfoSeq infoSeq;
        DDS_ReturnCode_t rc;

        
        DDSDynamicDataReader *controlReader = DDSDynamicDataReader::narrow(reader);
        if (controlReader == NULL) {
            std::cerr << "! Unable to narrow data reader" << std::endl;
            return;
        }

        rc = controlReader->take(
                            dataSeq, 
                            infoSeq, 
                            DDS_LENGTH_UNLIMITED,
                            DDS_ANY_SAMPLE_STATE, 
                            DDS_ANY_VIEW_STATE, 
                            DDS_ANY_INSTANCE_STATE);
        if (rc == DDS_RETCODE_NO_DATA) {
            return;
        } else if (rc != DDS_RETCODE_OK) {
            std::cerr << "! Unable to take data from data reader, error " 
                      << rc << std::endl;
            return;
        }

        for (int i = 0; i < dataSeq.length(); ++i) {
            if (infoSeq[i].valid_data) {
                if (current_command!=NONE){
                    std::cout<<"It is another command being processed. Discarting the new command."<<std::endl;
                } else {
                    // Process the data
                    DDS_DynamicData& instance = dataSeq[i];

                    //Deleting the previous command_name 
                    if (command_name!=NULL){
                        delete command_name;
                        command_name=NULL;
                    }
                    instance.get_string(command_name, NULL,
                            "command_name", 
                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);
                    
                    instance.get_short(command_id, 
                            "command_id", 
                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);
                    if (strcmp(command_name, "test_1")==0){
                        current_command = TEST_1;
                        operational_status = NOP;
                        objective_status = PROCESSING;
                        std::cout<<"A new test 1 command has been arrived."<<std::endl;

                    } else if (strcmp(command_name, "insuline_level")==0){
                        current_command = INSULINE_LEVEL;
                        objective_status = PROCESSING;
                        instance.get_long(insuline_level, 
                            "value", 
                            DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED);
                        current_insuline_level = 0;
                        std::cout<<"A new insuline level command has been arrived. Insuline level: "<<insuline_level<<std::endl;
                    }           
                }
            }
        }
    
        rc = controlReader->return_loan(dataSeq, infoSeq);
        if (rc != DDS_RETCODE_OK) {
            std::cerr << "! Unable to return loan, error "
                      << rc << std::endl;
        }
    }
};  // End of class ControlListener
/* Delete all entities */
static int appShutdown(
    DDSDomainParticipant * participant)
{
    DDS_ReturnCode_t retCode;
    int status = 0;

    if (participant != NULL) {
        retCode = participant->delete_contained_entities();
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_contained_entities error %d\n", retCode);
            status = -1;
        }

        retCode = DDSTheParticipantFactory->delete_participant(participant);
        if (retCode != DDS_RETCODE_OK) {
            fprintf(stderr, "delete_participant error %d\n", retCode);
            status = -1;
        }
    }
    
    /* Release memory associated with participant factory */
    retCode = DDSDomainParticipantFactory::finalize_instance();
    if (retCode != DDS_RETCODE_OK) {
        fprintf(stderr, "finalize_instance error %d\n", retCode);
        status = -1;
    }
    return status;
}


void changeObjState(DDS_DynamicData * obj_state_data_, short control_id_, OperationalStatus state_stage_, ObjectiveStatus state_){
    obj_state_data_->set_short("control_id", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, control_id_);
    obj_state_data_->set_long("state_stage", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, state_stage_);
    obj_state_data_->set_long("state", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, state_);
}

void changeCurrentState(DDS_DynamicData * current_data_, short control_id_, OperationalStatus state_stage_, char* state_name_, int value_=-1){
    current_data_->set_short("control_id", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, control_id_);
    current_data_->set_long("state_stage", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, state_stage_);
    current_data_->set_string("state_name", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, state_name_);
    current_data_->set_long("value", DDS_DYNAMIC_DATA_MEMBER_ID_UNSPECIFIED, value_);
}

int main(int argc, char *argv[])
{

    int sampleCount = 0; // infinite loop
    if (argc >= 2) {
        sampleCount = atoi(argv[2]);
    }

    DDS_ReturnCode_t retCode;
    int count = 0;  
    DDS_Duration_t status_change_period = {2,0};
    DDS_ReturnCode_t lastSetError = DDS_RETCODE_OK;

    DDS_DomainParticipantFactoryQos factory_qos;

    DDSTheParticipantFactory->get_qos(factory_qos);
    factory_qos.profile.url_profile.ensure_length(1, 1);
    factory_qos.profile.url_profile[0] = DDS_String_dup("hc_system.xml");
    DDSTheParticipantFactory->set_qos(factory_qos);

    ////////////////////////////////
    //Applications Creation from XML  
    /* Medical Device App Creation*/
    DDSDomainParticipant * md_participant = DDSTheParticipantFactory->
        create_participant_from_config("SCO_Components::MD_app");
    if (md_participant == NULL) {
        fprintf(stderr, "create_participant_from_config error MD_participant\n");
        appShutdown(md_participant);
        return -1;
    }

    ControlListener listener;
    ////////////////////////////////
    /* Control Reader */
    DDSDynamicDataReader* md_control_reader = DDSDynamicDataReader::narrow(
            md_participant->lookup_datareader_by_name(
                                      "MD_app_Sub::control_reader"));
    if (md_control_reader == NULL) {
        fprintf(stderr, "lookup_datareader_by_name error control_reader\n");
        appShutdown(md_participant);
        return -1;
    }

    md_control_reader->set_listener(&listener, DDS_STATUS_MASK_ALL);
  
    ////////////////////////////////
    /* Current Status Writer*/
    DDSDynamicDataWriter * md_current_writer = DDSDynamicDataWriter::narrow(
            md_participant->lookup_datawriter_by_name(
                                      "MD_app_Pub::current_state_writer"));
    if (md_current_writer == NULL) {
        fprintf(stderr, "lookup_datawriter_by_name error current_state_writer\n");
        appShutdown(md_participant);
        return -1;
    }
    DDS_DynamicData * current_data = 
            md_current_writer->create_data(DDS_DYNAMIC_DATA_PROPERTY_DEFAULT);
    if (current_data == NULL) {
        fprintf(stderr, "create_data error control_data\n");
        appShutdown(md_participant);
        return -1;
    }       
    ////////////////////////////////
     /* Objective status Writer*/
    DDSDynamicDataWriter * md_obj_state_writer = DDSDynamicDataWriter::narrow(
            md_participant->lookup_datawriter_by_name(
                                      "MD_app_Pub::objective_state_writer"));
    if (md_obj_state_writer == NULL) {
        fprintf(stderr, "lookup_datawriter_by_name error objective_state_writer\n");
        appShutdown(md_participant);
        return -1;
    }
    DDS_DynamicData * obj_state_data = 
            md_obj_state_writer->create_data(DDS_DYNAMIC_DATA_PROPERTY_DEFAULT);
    if (obj_state_data == NULL) {
        fprintf(stderr, "create_data error objective_state_data\n");
        appShutdown(md_participant);
        return -1;
    }       
    ////////////////////////////////        
     /* Streaming Writer*/
    DDSDynamicDataWriter * md_streaming_writer = DDSDynamicDataWriter::narrow(
            md_participant->lookup_datawriter_by_name(
                                      "MD_app_Pub::streaming_writer"));
    if (md_streaming_writer == NULL) {
        fprintf(stderr, "lookup_datawriter_by_name error streaming_writer\n");
        appShutdown(md_participant);
        return -1;
    }
    DDS_DynamicData * streaming_data = 
            md_streaming_writer->create_data(DDS_DYNAMIC_DATA_PROPERTY_DEFAULT);
    if (streaming_data == NULL) {
        fprintf(stderr, "create_data error streaming_writer\n");
        appShutdown(md_participant);
        return -1;
    }       

    ////////////////////////////////////////////////////////////////
    //Business Logic
    ////////////////////////////////////////////////////////////////
    /* Main loop */
    for (count=0; (sampleCount == 0) || (count < sampleCount); ++count) {
        
        if (listener.getCurrentControlCommand()==TEST_1){
            if (listener.getCurrentOperationalStatus()==NOP){
                
                listener.setCurrentOperationalStatus(IDLE);
                //Send a current state saying that the step IDLE is being executed
                changeCurrentState(current_data, listener.getCurrentCommandId(), IDLE, "IDLE");
                md_current_writer->write(*current_data, DDS_HANDLE_NIL);

                listener.setCurrentObjectiveStatus(PROCESSING);
                //Send a obj state sample saying that the IDLE step of the Control_id command is being PROCESSED.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), IDLE, PROCESSING);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);

                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" is being PROCESSED. The current status is 'IDLE'."<<std::endl;

            } else if (listener.getCurrentOperationalStatus()==IDLE){
                listener.setCurrentObjectiveStatus(SUCCESS);
                //Send a obj state sample saying that the IDLE step of the Control_id command has been SUCCESSfully executed.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), IDLE, SUCCESS);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);
                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" and status 'IDLE' has been successfully processed."<<std::endl;

                listener.setCurrentOperationalStatus(TAKING_SAMPLE);
                //Send a current state saying that the step TAKING_SAMPLE is being executed
                changeCurrentState(current_data, listener.getCurrentCommandId(), TAKING_SAMPLE, "TAKING SAMPLE");
                md_current_writer->write(*current_data, DDS_HANDLE_NIL);

                listener.setCurrentObjectiveStatus(PROCESSING);
                //Send a obj state sample saying that the TAKING_SAMPLE step of the Control_id command is being PROCESSED.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), TAKING_SAMPLE, PROCESSING);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);

                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" is being PROCESSED. The current status is 'TAKING_SAMPLE'."<<std::endl;

            } else if (listener.getCurrentOperationalStatus()==TAKING_SAMPLE){
                //Send a obj state sample saying that the TAKING_SAMPLE step of the Control_id command has been SUCCESSfully executed.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), TAKING_SAMPLE, SUCCESS);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);
                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" and status 'TAKING_SAMPLE' has been successfully processed."<<std::endl;

                listener.setCurrentOperationalStatus(ANALYZING);
                //Send a current state saying that the step ANALYZING is being executed
                changeCurrentState(current_data, listener.getCurrentCommandId(), ANALYZING, "ANALYZING SAMPLE");
                md_current_writer->write(*current_data, DDS_HANDLE_NIL);

                listener.setCurrentObjectiveStatus(PROCESSING);
                //Send a obj state sample saying that the ANALYZING step of the Control_id command is being PROCESSED.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), ANALYZING, PROCESSING);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);

                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" is being PROCESSED. The current status is 'ANALYZING'."<<std::endl;
            } else if (listener.getCurrentOperationalStatus()==ANALYZING){

                //Send a obj state sample saying that the ANALYZING step of the Control_id command has been SUCCESSfully executed.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), ANALYZING, SUCCESS);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);
                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" and status 'ANALYZING' has been successfully processed."<<std::endl;

                listener.setCurrentOperationalStatus(RESULTS);
                //Send a current state saying that the step ANALYZING is being executed
                changeCurrentState(current_data, listener.getCurrentCommandId(), RESULTS, "RESULTS");
                md_current_writer->write(*current_data, DDS_HANDLE_NIL);

                listener.setCurrentObjectiveStatus(PROCESSING);
                //Send a obj state sample saying that the ANALYZING step of the Control_id command is being PROCESSED.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), RESULTS, PROCESSING);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);

                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" is being PROCESSED. The current status is 'RESULTS'."<<std::endl;

            } else if (listener.getCurrentOperationalStatus()==RESULTS){

                //Send a obj state sample saying that the PROCESSING step of the Control_id command has been SUCCESSfully executed.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), RESULTS, SUCCESS);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);
                std::cout<<"The test_1 with id: "<<listener.getCurrentCommandId()<<" and status 'RESULTS' has been successfully processed. The test_1 command has been processed."<<std::endl;
                //Seting the current operational status to the initial one.
                listener.setCurrentOperationalStatus(NOP);

                //Setting the control command to None because the command has been successfully processed.
                listener.setCurrentControlCommand(NONE);
            }
        } else if (listener.getCurrentControlCommand()==INSULINE_LEVEL){
            if (listener.getCurrentInsulineLevel()<listener.getInsulineLevel()){
                
                listener.setCurrentInsulineLevel(listener.getCurrentInsulineLevel()+1);
                //Send a obj state sample saying that the Control_id command is being PROCESSED.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), NOP, PROCESSING);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);
                //Send a current state saying that the Insuline level is x.
                changeCurrentState(current_data, listener.getCurrentCommandId(), NOP, "current insuline level", listener.getCurrentInsulineLevel());
                md_current_writer->write(*current_data, DDS_HANDLE_NIL);

            } else if (listener.getCurrentInsulineLevel()==listener.getInsulineLevel()){
                listener.setCurrentObjectiveStatus(SUCCESS);
                //Send a obj state sample saying that the PROCESSING step of the Control_id command has been SUCCESSfully executed.
                changeObjState(obj_state_data, listener.getCurrentCommandId(), NOP, SUCCESS);
                md_obj_state_writer->write(*obj_state_data, DDS_HANDLE_NIL);
                std::cout<<"The insuline level command with id: "<<listener.getCurrentCommandId()<<" has been successfully processed."<<std::endl;
                //Seting the current operational status to the initial one.
                listener.setCurrentOperationalStatus(NOP);

                //Setting the control command to None because the command has been successfully processed.
                listener.setCurrentControlCommand(NONE);
            }
        }
        NDDSUtility::sleep(status_change_period);
    }

    ////////////////////////////////////////////////////////////////
    /* Delete data samples */
    md_current_writer->delete_data(current_data);
    md_obj_state_writer->delete_data(obj_state_data);
    md_streaming_writer->delete_data(streaming_data);

    /* Delete all entities */
    return appShutdown(md_participant);
}



