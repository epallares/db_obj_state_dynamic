# DOCBOX PoC XML APP Creation - Service and Control Object Module definition

## Example of a complex xml_app_creation project

## HOWTO Build

Navigate to the folder where you unzip the reproducer and run CMake for the
desired architecture.
```
mkdir build
cd build
cmake .. -DARCHITECTURE=<arch>
```
If Windows and x64Win64VS2015:
```
cmake .. -DCONNEXTDDS_ARCH=x64Win64VS2015 -G "Visual Studio 14 Win64" 
```
Use the following directive to build for Release/Debug:
```
-DCMAKE_BUILD_TYPE=<Release/Debug>
```
Use the following directive to build static/dynamic libraries:
```
-DLINK_MODE=<Static/Dynamic>
```
In Windows, it is recommended to use this command to build the solution:
```
cmake --build . --config <Release/Debug>
```

# For example build the architecture x64Win64VS2015:

```
mkdir build
cd build
cmake .. -DCONNEXTDDS_ARCH=x64Win64VS2015 -G "Visual Studio 14 Win64" -DCMAKE_BUILD_TYPE=Debug 
cmake --build . --config Debug
```

# HOWTO Run on C++

Run the following command from the reproducer directory to execute the
application.

## On *UNIX* systems:
Using 3 different consoles, run:
```
./build/control_app
```

```
./build/medical_device
```

```
./build/monitoring
```

## On *Windows* Systems:
Using two different consoles, run:
```
build\<Relase/Debug>\app_publisher.exe
```
and
```
build\<Relase/Debug>\app_subscriber.exe
```